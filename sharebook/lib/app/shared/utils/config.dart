import 'package:flutter/foundation.dart';

const BASE_URL_DEBUG = "http://localhost:8000/graphql/";
const BASE_URL_RELEASE = "https://sb-eng.herokuapp.com/graphql/";

class Config {
  static final Config _singleton = new Config._internal();
  String _url = BASE_URL_RELEASE;

  factory Config() {
    return _singleton;
  }

  String get url => _url;

  Config._internal() {
    this._url = kReleaseMode ? BASE_URL_RELEASE : BASE_URL_DEBUG;
  }
}
