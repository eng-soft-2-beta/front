import 'package:shared_preferences/shared_preferences.dart';

abstract class ITokenLocalStorage {
  Future<String> getToken();
  Future<void> saveToken(String token);
  Future<void> deleteToken();
  String _tokenValue = "";
  String get token => _tokenValue;
}

const TOKEN_KEY = 'token-key';

class TokenLocalStorage implements ITokenLocalStorage {
  var _tokenValue = "";

  TokenLocalStorage() {
    SharedPreferences.getInstance();
  }

  String get token => _tokenValue;

  bool get hasToken => _tokenValue.isNotEmpty;

  @override
  Future<String> getToken() async {
    var storage = await SharedPreferences.getInstance();
    _tokenValue = storage.getString(TOKEN_KEY) as String;
    return 'JWT $_tokenValue';
  }

  @override
  Future<void> saveToken(String token) async {
    var storage = await SharedPreferences.getInstance();
    await storage.setString(TOKEN_KEY, token);
  }

  @override
  Future<void> deleteToken() async {
    var storage = await SharedPreferences.getInstance();
    _tokenValue = "";
    storage.remove(TOKEN_KEY);
  }
}
