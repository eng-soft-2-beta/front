// import 'package:hasura_connect/hasura_connect.dart';

import 'package:graphql/client.dart';
import 'package:sharebook/app/shared/graphql/graphql_client.dart';

class LoginRepository {
  final NotAuthenticatedGraphQLClient client;

  LoginRepository(this.client);

  Future<String> login(String username, String password) async {
    var query = """
      mutation login(\$username:String!, \$password:String!) {
        tokenAuth(username: \$username, password: \$password) {
          token
        }
      }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: {"username": username, "password": password},
    );

    final QueryResult result = await client.mutate(options);

    if (result.hasException) {
      List<GraphQLError> errors =
          result.exception?.graphqlErrors ?? List<GraphQLError>.empty();

      GraphQLError firstError = errors.length > 0
          ? errors.first
          : GraphQLError(message: "Houve um erro interno no servidor!");
      throw firstError;
    } else {
      return result.data?['tokenAuth']?["token"];
    }
  }
}
