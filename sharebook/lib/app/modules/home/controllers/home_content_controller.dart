import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/models/book_model.dart';
import 'package:sharebook/app/modules/home/repositories/home_content_repository.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';

import '../../../app_routes.dart';

class HomeContentController extends GetxController {
  final _homeContentRepository = Get.find<HomeContentRepository>();

  final localStorage = Get.find<TokenLocalStorage>();

  final bookSearchInput = TextEditingController();

  var isSearchingBooks = false.obs;

  List<BookModel> booksResult = [];

  bool get hasToken => localStorage.hasToken;

  HomeContentController() {
    isSearchingBooks.value = false;
  }

  void loginPage() {
    Get.toNamed(Routes.loginPage);
  }

  void logoutPage() {
    localStorage.deleteToken();
  }

  void registerPage() {
    Get.toNamed(Routes.registerPage);
  }

  void searchBooksPage(BuildContext context) async {
    booksResult =
        await _homeContentRepository.searchBooks(bookSearchInput.text);
    print("### Books result: $booksResult");
    isSearchingBooks.value = true;
    // Navigator.of(context).pop();
  }
}
