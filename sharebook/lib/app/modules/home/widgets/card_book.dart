import 'package:flutter/material.dart';

class CardBook extends StatelessWidget {
  final Function? onTap;
  final String? title;

  const CardBook({Key? key, this.onTap, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () => onTap!(),
        child: SizedBox(
          height: 280,
          width: 280,
          child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              color: Colors.grey,
            ),
            child: Text(title ?? ""),
          ),
        ),
      ),
    );
  }
}
