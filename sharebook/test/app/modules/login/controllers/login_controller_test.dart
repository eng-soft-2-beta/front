import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/modules/login/controllers/login_controller.dart';
import 'package:mockito/annotations.dart';
import 'package:sharebook/app/modules/login/repositories/login_repository.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';
import 'login_controller_test.mocks.dart';

@GenerateMocks([LoginRepository, TokenLocalStorage])
void main() {
  // Mocks
  final MockLoginRepository mockLoginRepository = MockLoginRepository();
  final MockTokenLocalStorage mockTokenLocalStorage = MockTokenLocalStorage();

  // Classe a ser testada
  final LoginController loginController =
      LoginController(mockLoginRepository, mockTokenLocalStorage);

  setUp(() {
    when(mockLoginRepository.login("user@teste.com", "@123456"))
        .thenAnswer((_) async => MOCK_TOKEN);
    when(mockTokenLocalStorage.getToken()).thenAnswer((_) async => MOCK_TOKEN);
  });

  test("Deve realizar login com sucesso", () {
    loginController.inputUsername.text = "user@teste.com";
    loginController.inputPassword.text = "@123456";
    loginController.login();
    expect(loginController.loginError.value, "");
  });
}

const String MOCK_TOKEN =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Imx1Y2FzQHVmYmEuYnIiLCJleHAiOjE2MjE0OTAxMDMsIm9yaWdJYXQiOjE2MjE0NTQxMDN9.A72koMCSVE-5i-Mtawmlt-tYFj6Ss81pmnJQCgMPt3g";
