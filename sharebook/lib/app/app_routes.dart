import 'package:get/get.dart';
import 'app_binding.dart';
import 'modules/books/pages/books_page.dart';
import 'modules/home/pages/home_page.dart';
import 'modules/login/pages/login_page.dart';
import 'modules/register/pages/register_page.dart';

abstract class Routes {
  static const String initial = '/';
  static const String loginPage = '/login';
  static const String registerPage = '/register';
  static const String booksPage = '/books';
}

mixin AppRoutes {
  static List<GetPage> routes = [
    GetPage(
      name: Routes.initial,
      page: () => HomePage(),
    ),
    GetPage(
      name: Routes.loginPage,
      page: () => LoginPage(),
      binding: AppBinding(),
    ),
    GetPage(
      name: Routes.registerPage,
      page: () => RegisterPage(),
    ),
    GetPage(
      name: Routes.booksPage,
      page: () => BooksPage(),
    ),
  ];
}
