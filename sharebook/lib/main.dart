//import 'package:url_strategy/url_strategy.dart';
import 'package:flutter/material.dart';
import 'app/app_binding.dart';
import 'app/app_widget.dart';
import 'app/shared/utils/config.dart';

void main() {
  Config();
  // setPathUrlStrategy();
  AppBinding().dependencies();
  runApp(ShareBook());
}
