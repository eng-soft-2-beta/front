import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final TextEditingController inputController;
  final Function searchPage;

  const SearchBar(
      {Key? key, required this.inputController, required this.searchPage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 15.0),
          width: MediaQuery.of(context).size.width * 0.3,
          height: 50.0,
          decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                // BoxShadow(
                //   color: Colors.grey.withOpacity(0.5),
                //   spreadRadius: 5,
                //   blurRadius: 7,
                //   offset: Offset(0, 3), // changes position of shadow
                // ),
              ]),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.01),
                  child: Icon(Icons.search)),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.01),
                width: MediaQuery.of(context).size.width * 0.25,
                child: TextFormField(
                  controller: inputController,
                  decoration: InputDecoration(
                    labelText: 'Digite o título que deseja achar',
                    border: InputBorder.none,
                  ),
                  validator: (String? value) {
                    if (value!.isEmpty) return 'Insira um title válido!';
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.08,
          decoration: BoxDecoration(
              // color: Colors.black12,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                // BoxShadow(
                //   color: Colors.grey.withOpacity(0.5),
                //   spreadRadius: 5,
                //   blurRadius: 7,
                //   offset: Offset(0, 3), // changes position of shadow
                // ),
              ]),
          child: TextButton(
            child: Text(
              'Pesquisar Livro',
              style: TextStyle(fontSize: 17.0),
            ),
            onPressed: () async {
              searchPage(context);
            },
          ),
        ),
      ],
    );
  }
}
