import 'dart:convert';

class PersonModel {
  PersonModel({
    this.name,
    this.id,
  });

  String? name;
  int? id;

  factory PersonModel.fromJson(String str) =>
      PersonModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PersonModel.fromMap(Map<String, dynamic> json) => PersonModel(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : int.parse(json["id"]),
      );

  Map<String, dynamic> toMap() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
      };
}
