import 'package:graphql/client.dart';
import 'package:mockito/annotations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/modules/login/repositories/login_repository.dart';
import 'package:sharebook/app/shared/graphql/graphql_client.dart';
import 'login_repository_test.mocks.dart';

@GenerateMocks([NotAuthenticatedGraphQLClient])
void main() {
  // Mocks
  final MockNotAuthenticatedGraphQLClient client =
      MockNotAuthenticatedGraphQLClient();
  // Classe a ser testada
  final LoginRepository repository = LoginRepository(client);

  setUp(() {
    final QueryResult queryResult = QueryResult(
      source: QueryResultSource.network,
      data: {
        "__typename": "Mutation",
        "tokenAuth": {
          "__typename": "ObtainJSONWebToken",
          "token": MOCK_TOKEN,
          // "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Imx1Y2FzQHVmYmEuYnIiLCJleHAiOjE2MjIwODAzNTgsIm9yaWdJYXQiOjE2MjIwNDQzNTh9.ZEWYVnDXEjGl5GIWAlQ-oWo-R-AiQSzNlZ5JUZ9FXQY"
        }
      },
      exception: null,
    );

    when(client.mutate(any)).thenAnswer((_) async => queryResult);
  });

  test("Deve realizar login com sucesso.", () async {
    final response = await repository.login("user@teste.com", "123456");
    expect(response, MOCK_TOKEN);
  });
  test("Deve retornar erro de credenciais inválidas.", () async {
    final response = await repository.login("user@teste.com", "000");
    expect(response, MOCK_TOKEN);
  });
}

const String MOCK_TOKEN =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Imx1Y2FzQHVmYmEuYnIiLCJleHAiOjE2MjE0OTAxMDMsIm9yaWdJYXQiOjE2MjE0NTQxMDN9.A72koMCSVE-5i-Mtawmlt-tYFj6Ss81pmnJQCgMPt3g";
