import 'package:graphql/client.dart';

class CustomMutation {
  final HttpLink httpLink;
  final AuthLink authLink;

  CustomMutation(this.httpLink, this.authLink);
}
