import 'package:flutter_test/flutter_test.dart';
import 'package:graphql/client.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/models/person_model.dart';
import 'package:sharebook/app/modules/register/repositories/register_repository.dart';
import 'package:sharebook/app/shared/graphql/graphql_client.dart';
import 'register_repository_test.mocks.dart';

@GenerateMocks([NotAuthenticatedGraphQLClient])
void main() {
  // Mocks
  final MockNotAuthenticatedGraphQLClient client =
      MockNotAuthenticatedGraphQLClient();

  // Classe a ser testada
  final RegisterRepository repository = RegisterRepository(client);

  setUp(() {
    final QueryResult queryResult = QueryResult(
      source: QueryResultSource.network,
      data: {
        "__typename": "Mutation",
        "createPerson": {
          "__typename": "CreatePerson",
          "person": {
            "__typename": "PersonType",
            "name": "Lucas Moreira",
            "id": "41"
          }
        }
      },
      exception: null,
    );
    when(client.mutate(any)).thenAnswer((_) async => queryResult);
  });

  test("Deve realizar o registro de um usuário", () async {
    var response = await repository.singup(
      "Joao Silva",
      "user@teste.com",
      "@123456",
      "123456789",
    );

    expect(response, isA<PersonModel>());
  });
}
