import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/models/person_model.dart';
import 'package:sharebook/app/modules/register/controllers/register_controller.dart';
import 'package:sharebook/app/modules/register/repositories/register_repository.dart';
import 'register_controller_test.mocks.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([RegisterRepository])
void main() {
  // Mocks
  final MockRegisterRepository mockRegisterRepository =
      MockRegisterRepository();

  // Classe a ser testada
  final RegisterController registerController =
      RegisterController(mockRegisterRepository);

  setUp(() {
    Get.testMode = true;
    when(mockRegisterRepository.singup(any, any, any, any))
        .thenAnswer((_) async => PersonModel());
  });

  test('Deve realizar o cadastro do usuário com sucesso', () {
    registerController.name.value = "User da Silva";
    registerController.cpf.value = "1234678910";
    registerController.username.value = "user@teste.com";
    registerController.password.value = "@123456";
    registerController.singup();
    expect(registerController.loginError.value, "");
  });
}
