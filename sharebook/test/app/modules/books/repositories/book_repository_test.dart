import 'package:flutter_test/flutter_test.dart';
import 'package:graphql/client.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/modules/books/repositories/books_repository.dart';
import 'package:sharebook/app/shared/graphql/graphql_client.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';
import 'book_repository_test.mocks.dart';

@GenerateMocks([AuthenticatedGraphQLClient, TokenLocalStorage])
void main() {
  // Mocks
  final MockAuthenticatedGraphQLClient client =
      MockAuthenticatedGraphQLClient();
  final MockTokenLocalStorage localStorage = MockTokenLocalStorage();

  // Classe a ser testada
  final BooksRepository repository = BooksRepository(client, localStorage);

  setUp(() {
    final QueryResult queryResult = QueryResult(
      source: QueryResultSource.network,
      data: {
        "__typename": "Query",
        "createPerson": {
          "__typename": "getPerson",
          "library": {
            {
              "__typename": "PersonType",
              "books": [
                {
                  "id": "2",
                  "title": "Segundo livro",
                  "autor": "Lucas Ribeiro",
                  "publishingCompany": "UFBA",
                  "isbn": "11122223",
                  "status": "DISPONIVEL"
                },
                {
                  "id": "3",
                  "title": "Terceiro livro",
                  "autor": "Lucas Ribeiro",
                  "publishingCompany": "UFBA",
                  "isbn": "1112882223",
                  "status": "DISPONIVEL"
                },
              ],
            },
          }
        }
      },
      exception: null,
    );
    when(client.query(any)).thenAnswer((_) async => queryResult);
  });

  test("Deve listar livros cadastrados", () async {
    var response = await repository.listBooks();
    print('response: $response');
    expect(response, isA<List<Map>>());
  });

  test("Deve realizar o cadastro de um livro", () {
    // code
  });
}
