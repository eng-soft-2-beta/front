import 'package:flutter/material.dart';

class ServerMessage {
  static Text error(String message) {
    return Text(
      message,
      style: TextStyle(color: Colors.red),
    );
  }
}
