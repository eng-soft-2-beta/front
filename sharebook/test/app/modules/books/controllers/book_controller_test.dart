import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:mockito/mockito.dart';
import 'package:sharebook/app/modules/books/controllers/books_controller.dart';
import 'package:sharebook/app/modules/books/repositories/books_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';
import 'book_controller_test.mocks.dart';

@GenerateMocks([BooksRepository, TokenLocalStorage])
void main() {
  // Mocks
  final MockBooksRepository repository = MockBooksRepository();
  final MockTokenLocalStorage localStorage = MockTokenLocalStorage();

  // Classe a ser testada
  final BooksController controller = BooksController(localStorage, repository);

  setUp(() {
    // code
  });

  test("Deve realizar o cadastro de um livro", () {
    // code
  });
}
