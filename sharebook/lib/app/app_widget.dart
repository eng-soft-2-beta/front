import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/app_routes.dart';

import 'app_binding.dart';

class ShareBook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      defaultTransition: Transition.noTransition,
      debugShowCheckedModeBanner: false,
      initialBinding: AppBinding(),
      title: 'Share Book',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: Routes.initial,
      getPages: AppRoutes.routes,
    );
  }
}
