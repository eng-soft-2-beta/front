import 'package:get/get.dart';

import '../../../app_routes.dart';

class HomeController extends GetxController {

  var selectedIndex = 0.obs;
  void updateIndex(int index) {
    selectedIndex.value = index;
  }
}
