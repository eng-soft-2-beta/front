import 'package:graphql/client.dart';
import 'package:sharebook/app/models/book_model.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';

class HomeContentRepository {
  final GraphQLClient client;
  final TokenLocalStorage storage;

  HomeContentRepository(this.client, this.storage);

  Future<List<BookModel>> searchBooks(String titleString) async {
    var query = """
      query LivrosDisponiveis(\$titleString: String!){
        getAvailableBooks(title:\$titleString){
            isbn
            id
            title
            autor
            publishingCompany
            status
        }
      }
    """;

    final QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {"titleString": titleString},
    );

    final QueryResult result = await client.query(options);

    var response = result.data?["getAvailableBooks"];
    return List<BookModel>.from(response.map((e) => BookModel.fromMap(e)));
  }
}
