import 'package:get/get.dart';
import 'package:graphql/client.dart';
import 'package:sharebook/app/modules/home/repositories/home_content_repository.dart';
import 'package:sharebook/app/modules/login/repositories/login_repository.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';
import 'package:sharebook/app/shared/utils/config.dart';
import 'modules/books/controllers/books_controller.dart';
import 'modules/books/repositories/books_repository.dart';
import 'modules/home/controllers/home_content_controller.dart';
import 'modules/home/controllers/home_controller.dart';
import 'modules/login/controllers/login_controller.dart';
import 'modules/register/controllers/register_controller.dart';
import 'modules/register/repositories/register_repository.dart';
import 'shared/graphql/graphql_client.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(TokenLocalStorage(), permanent: true);
    Get.lazyPut(
      () => AuthenticatedGraphQLClient(
        link: HttpLink(Config().url),
        authLink: AuthLink(getToken: Get.find<TokenLocalStorage>().getToken),
        cache: GraphQLCache(),
      ),
    );
    Get.put(
      NotAuthenticatedGraphQLClient(
        link: HttpLink(Config().url),
        cache: GraphQLCache(),
      ),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<HomeContentController>(
      () => HomeContentController(),
    );
    Get.lazyPut<HomeContentRepository>(
      () => HomeContentRepository(Get.find<AuthenticatedGraphQLClient>(),
          Get.find<TokenLocalStorage>()),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(
        Get.find<LoginRepository>(),
        Get.find<TokenLocalStorage>(),
      ),
    );
    Get.lazyPut<LoginRepository>(
      () => LoginRepository(
        Get.find<NotAuthenticatedGraphQLClient>(),
      ),
    );
    Get.lazyPut<RegisterController>(
      () => RegisterController(Get.find<RegisterRepository>()),
    );
    Get.lazyPut<RegisterRepository>(
      () => RegisterRepository(
        Get.find<NotAuthenticatedGraphQLClient>(),
      ),
    );
    Get.lazyPut<BooksController>(
      () => BooksController(
        Get.find<TokenLocalStorage>(),
        Get.find<BooksRepository>(),
      ),
    );
    Get.lazyPut<BooksRepository>(
      () => BooksRepository(
        Get.find<AuthenticatedGraphQLClient>(),
        Get.find<TokenLocalStorage>(),
      ),
    );
  }
}
