import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/modules/books/pages/books_page.dart';
import 'package:sharebook/app/modules/home/controllers/home_controller.dart';
import 'package:sharebook/app/modules/home/widgets/tab_button_widget.dart';

import 'home_content_page.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Obx(
          () => Row(
            children: [
              NavigationRail(
                minWidth: 100.0,
                backgroundColor: Colors.blue.shade600,
                selectedIndex: controller.selectedIndex.value,
                onDestinationSelected: controller.updateIndex,
                labelType: NavigationRailLabelType.all,
                destinations: [
                  TabButtonWidget("Home", Icons.home),
                  TabButtonWidget("Meus\nLivros", Icons.book),
                  TabButtonWidget("Perfil", Icons.book),
                ],
              ),
              pageView(context),
            ],
          ),
        ),
      ),
    );
  }

  final List<Widget> pages = [
    HomeContentPage(),
    BooksPage(),
    Center(child: Text("Page 3")),
  ];

  Expanded pageView(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 8, 0, 0),
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: pages[controller.selectedIndex.value],
        ),
      ),
    );
  }
}
