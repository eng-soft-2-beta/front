import 'package:graphql/client.dart';
import 'package:sharebook/app/models/person_model.dart';
import 'package:sharebook/app/shared/graphql/graphql_client.dart';

class RegisterRepository {
  final NotAuthenticatedGraphQLClient client;

  RegisterRepository(this.client);

  Future<PersonModel> singup(
    String? name,
    String? username,
    String password,
    String cpf,
  ) async {
    final query = """
      mutation criarPessoa(\$name:String!, \$email:String!, \$cpf:String!, \$password:String!) {
        createPerson (accInput: {
          email: \$email,
          password: \$password,
          phoneNumber: "71998877655"
        },
        addInput: {
          city:"salvador",
          street: "Av sete",
          number: "10",
          complement: "Apt 109"
        },
          persInput: {
            name:  \$name,
            lastname: "da Silva",
            cpf: \$cpf,
            cnh:"123456789"
          }
        ){
          person {
            name, id
          }
        }
      }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: <String, dynamic>{
        'name': name,
        'email': username,
        'password': password,
        'cpf': cpf,
      },
    );

    final QueryResult result = await client.mutate(options);

    if (result.hasException) {
      print(result.exception.toString());
      List<GraphQLError> errors =
          result.exception?.graphqlErrors ?? List<GraphQLError>.empty();
      GraphQLError firstError = errors.length > 0
          ? errors.first
          : GraphQLError(message: "Houve um erro interno no servidor!");
      throw firstError;
    } else {
      var person = result.data?['createPerson']['person'];
      return PersonModel.fromMap(person);
    }
  }
}
