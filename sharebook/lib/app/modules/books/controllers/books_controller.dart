import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:sharebook/app/models/book_model.dart';
import 'package:sharebook/app/modules/books/repositories/books_repository.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';

import '../../../app_routes.dart';

class BooksController extends GetxController {
  var books = List<BookModel>.empty().obs;
  final titleInput = TextEditingController();
  final autorInput = TextEditingController();
  final publishingCompanyInput = TextEditingController();
  final isbnInput = TextEditingController();
  var authenticated = false.obs;

  final TokenLocalStorage localStorage;
  final BooksRepository _booksRepository;

  BooksController(this.localStorage, this._booksRepository) {
    localStorage
        .getToken()
        .then((value) => authenticated.value = value.isNotEmpty);

    _booksRepository.listBooks().then((value) => books.value = value);
  }

  _updateStatus(int index, String status) async {
    BookModel book = books[index];
    book.status = status;
    books[index] = book;
    await _booksRepository.updateStatus(books[index].id, books[index].status);
  }

  setPrivate(int index) {
    _updateStatus(index, "PRIVADO");
  }

  setDisponivel(int index) {
    _updateStatus(index, "DISPONIVEL");
  }

  setEmprestado(int index) {
    _updateStatus(index, "EMPRESTADO");
  }

  registerBook() async {
    BookModel book = new BookModel();
    book.title = titleInput.text;
    book.autor = autorInput.text;
    book.publishingCompany = publishingCompanyInput.text;
    book.isbn = isbnInput.text;
    book.status = "DISPONIVEL";
    books.add(await _booksRepository.registerBook(book));
  }

  editBook(int index) async {
    BookModel book = books[index];
    book.title = titleInput.text;
    book.autor = autorInput.text;
    book.publishingCompany = publishingCompanyInput.text;
    book.isbn = isbnInput.text;
    books[index] = await _booksRepository.updateBook(book);
  }

  removeBook(String id) async {
    books.removeWhere((item) => item.id.toString() == id);
    await _booksRepository.removeBook(id);
  }

  listBooks() {
    _booksRepository.listBooks();
  }

  bool isNumericOnly(String? value) {
    return GetUtils.isNumericOnly(value ?? "");
  }

  void loginPage() {
    Get.toNamed(Routes.loginPage);
  }

  resetInputs() {
    titleInput.text = "";
    autorInput.text = "";
    publishingCompanyInput.text = "";
    isbnInput.text = "";
  }

  fillInputs(int index) {
    BookModel book = books[index];
    titleInput.text = book.title.toString();
    autorInput.text = book.autor.toString();
    publishingCompanyInput.text = book.publishingCompany.toString();
    isbnInput.text = book.isbn.toString();
  }
}
