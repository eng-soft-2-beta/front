import 'package:get/get.dart';
import 'package:graphql/client.dart';
import 'package:sharebook/app/models/person_model.dart';
import 'package:sharebook/app/modules/register/repositories/register_repository.dart';

import '../../../app_routes.dart';

class RegisterController extends GetxController {
  var name = ''.obs;
  var cpf = ''.obs;
  var username = ''.obs;
  var password = ''.obs;
  var loginError = ''.obs;

  final RegisterRepository registerRepository;

  RegisterController(this.registerRepository);

  bool emailIsValid(String? value) {
    return GetUtils.isEmail(value ?? "");
  }

  singup() async {
    try {
      PersonModel person = await registerRepository.singup(
        this.name.value,
        this.username.value,
        this.password.value,
        this.cpf.value,
      );
      print('controller person: $person');
      Get.toNamed(Routes.loginPage);
    } on GraphQLError catch (e) {
      print('Register GraphQLError: ${e.message}');
      loginError.value = e.message;
    } catch (e) {
      print('Register error: $e');
      loginError.value = "Error interno no servidor.";
    }
  }
}
