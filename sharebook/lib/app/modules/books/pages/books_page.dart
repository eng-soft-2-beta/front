import 'package:data_tables/data_tables.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/modules/books/controllers/books_controller.dart';
import 'package:sharebook/app/modules/books/widgets/action_button_widget.dart';

// ignore: must_be_immutable

class BooksPage extends GetView<BooksController> {
  final List<DataColumn> dataColumns = [
    DataColumn(
      label: const Text('ID', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Título', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Autor', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Editora', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('ISBN', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Ações', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.authenticated.value
        ? _authenticatedPage(context)
        : _unauthenticatedPage(context));
  }

  Scaffold _authenticatedPage(BuildContext context) => Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: ActionButton(
          onPressed: () async {
            return showBookFormAlert(context, controller.registerBook);
          },
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.8,
            padding:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
            child: NativeDataTable.builder(
              columns: dataColumns,
              itemCount: controller.books.length,
              itemBuilder: (int index) {
                final book = controller.books[index];
                return DataRow(
                  cells: [
                    DataCell(Text(
                      book.id.toString(),
                      style: TextStyle(
                          fontSize: 19,
                          color: book.status.toString() == "EMPRESTADO"
                              ? Colors.red
                              : Colors.black),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                    DataCell(Text(
                      book.title.toString(),
                      style: TextStyle(
                          fontSize: 19,
                          color: book.status.toString() == "EMPRESTADO"
                              ? Colors.red
                              : Colors.black),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                    DataCell(Text(
                      book.autor.toString(),
                      style: TextStyle(
                          fontSize: 19,
                          color: book.status.toString() == "EMPRESTADO"
                              ? Colors.red
                              : Colors.black),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                    DataCell(Text(
                      book.publishingCompany.toString(),
                      style: TextStyle(
                          fontSize: 19,
                          color: book.status.toString() == "EMPRESTADO"
                              ? Colors.red
                              : Colors.black),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                    DataCell(Text(
                      book.isbn.toString(),
                      style: TextStyle(
                          fontSize: 19,
                          color: book.status.toString() == "EMPRESTADO"
                              ? Colors.red
                              : Colors.black),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                    DataCell(
                      Row(
                        children: [
                          book.isEmprestado
                              ? Tooltip(
                                  message:
                                      "Impossível remover. Livro emprestado.",
                                  child: Icon(
                                    Icons.remove,
                                    size: 28,
                                  ),
                                )
                              : Tooltip(
                                  message: "Remover livro.",
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.remove,
                                      size: 28,
                                    ),
                                    onPressed: () async {
                                      controller.removeBook(book.id.toString());
                                    },
                                  ),
                                ),
                          book.isEmprestado
                              ? Tooltip(
                                  message:
                                      "Impossível editar. Livro emprestado.",
                                  child: Icon(
                                    Icons.edit,
                                    size: 28,
                                  ),
                                )
                              : Tooltip(
                                  message: "Editar livro.",
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.edit,
                                      size: 28,
                                    ),
                                    onPressed: () async {
                                      controller.fillInputs(index);
                                      return showBookFormAlert(
                                          context, controller.editBook);
                                    },
                                  ),
                                ),
                          Tooltip(
                            message: "Definir privacidade.",
                            child: IconButton(
                              icon: Icon(
                                book.isDisponivel
                                    ? Icons.lock_open_outlined
                                    : Icons.lock_rounded,
                                size: 28,
                              ),
                              onPressed: book.isEmprestado
                                  ? null
                                  : book.isDisponivel
                                      ? () => controller.setPrivate(index)
                                      : () => controller.setDisponivel(index),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      );

  Scaffold _unauthenticatedPage(BuildContext context) => Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Usuário não autenticado."),
              SizedBox(height: 10),
              ElevatedButton(
                onPressed: controller.loginPage,
                child: Text("Realizar login"),
              ),
            ],
          ),
        ),
      );

  showBookFormAlert(BuildContext context, Function onPressed,
      {int index = -1}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Cadastrar Livro'),
          buttonPadding:
              EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.025),
          content: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.35,
                child: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      TextFormField(
                        controller: controller.titleInput,
                        maxLength: 150,
                        decoration: InputDecoration(
                            labelText: 'Digite o título do livro'),
                        validator: (String? value) {
                          if (value!.isEmpty) return 'Insira um title válido!';
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        controller: controller.autorInput,
                        maxLength: 150,
                        decoration: InputDecoration(
                          labelText: 'Digite o autor do livro',
                        ),
                        textCapitalization: TextCapitalization.words,
                        validator: (String? value) {
                          if (value!.isEmpty) return 'Insira um Autor válido!';
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        controller: controller.publishingCompanyInput,
                        maxLength: 180,
                        decoration: InputDecoration(
                            labelText: 'Digite a editora do livro'),
                        validator: (String? value) {
                          if (value!.isEmpty)
                            return 'Insira uma editora válido!';
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        controller: controller.isbnInput,
                        maxLength: 50,
                        decoration: InputDecoration(labelText: 'Digite o ISBN'),
                        validator: (String? value) {
                          if (!controller.isNumericOnly(value))
                            return 'Insira um ISBN válido!';
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          actionsPadding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.015),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancelar',
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                controller.resetInputs();
                Navigator.of(context).pop();
              },
            ),
            SizedBox(width: MediaQuery.of(context).size.width * 0.005),
            TextButton(
              child: Text('Cadastrar Livro'),
              onPressed: () async {
                if (index >= 0) {
                  onPressed(index);
                } else {
                  onPressed();
                }
                controller.resetInputs();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
