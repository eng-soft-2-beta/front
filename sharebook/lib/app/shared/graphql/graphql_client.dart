import 'package:graphql/client.dart';

class AuthenticatedGraphQLClient extends GraphQLClient {
  final Link link;
  final AuthLink authLink;
  final GraphQLCache cache;

  AuthenticatedGraphQLClient({
    required this.link,
    required this.authLink,
    required this.cache,
  }) : super(link: authLink.concat(link), cache: cache);
}

class NotAuthenticatedGraphQLClient extends GraphQLClient {
  final Link link;
  final GraphQLCache cache;

  NotAuthenticatedGraphQLClient({
    required this.link,
    required this.cache,
  }) : super(link: link, cache: cache);
}
