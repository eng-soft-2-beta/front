import 'package:data_tables/data_tables.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/modules/home/controllers/home_content_controller.dart';
import 'package:sharebook/app/modules/home/widgets/search_bar.dart';

class HomeContentPage extends GetView<HomeContentController> {
  final List<DataColumn> dataColumns = [
    DataColumn(
      label: const Text('ID', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Titulo', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Autor', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Editora', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('ISBN', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Ações', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
    DataColumn(
      label: const Text('Proprietário', style: TextStyle(fontSize: 22)),
      // onSort: (int columnIndex, bool ascending) =>
      // _sort<String>((Dessert d) => d.name, columnIndex, ascending),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() {
        if (controller.isSearchingBooks.value)
          return _bookSearchView(context);
        else
          return _bookHomeView(context);
      }),
      floatingActionButton: floatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Widget _bookHomeView(BuildContext context) {
    return Container(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.3),
      child: Center(
        child: SearchBar(
          inputController: controller.bookSearchInput,
          searchPage: controller.searchBooksPage,
        ),
      ),
    );
  }

  Widget _bookSearchView(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.9,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 30.0, left: 50.0),
            child: SearchBar(
              inputController: controller.bookSearchInput,
              searchPage: controller.searchBooksPage,
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 20.0),
              height: 1.0,
              width: 1760.0,
              color: Colors.black),
          Flexible(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02),
                child: NativeDataTable.builder(
                  columns: dataColumns,
                  itemCount: controller.booksResult.length,
                  itemBuilder: (int index) {
                    return DataRow(
                      cells: [
                        DataCell(Text("${controller.booksResult[index].id}",
                            style: TextStyle(fontSize: 19))),
                        DataCell(Text("${controller.booksResult[index].title}",
                            style: TextStyle(fontSize: 19))),
                        DataCell(Text("${controller.booksResult[index].autor}",
                            style: TextStyle(fontSize: 19))),
                        DataCell(Text(
                            "${controller.booksResult[index].publishingCompany}",
                            style: TextStyle(fontSize: 19))),
                        DataCell(Text("${controller.booksResult[index].isbn}",
                            style: TextStyle(fontSize: 19))),
                        DataCell(
                          Row(
                            children: [
                              Tooltip(
                                message: "Solicitar Empréstimo",
                                child: Icon(
                                  Icons.menu_book,
                                  size: 28,
                                ),
                              ),
                              Tooltip(
                                message: "Reservar Exemplar",
                                child: Icon(
                                  Icons.calendar_today_rounded,
                                  size: 28,
                                ),
                              ),
                            ],
                          ),
                        ),
                        DataCell(
                          Text("Fulano da Silva",
                              style: TextStyle(
                                  fontSize: 19,
                                  color: Colors.blueAccent,
                                  decoration: TextDecoration.underline)),
                        )
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget floatingActionButton() {
    return controller.hasToken
        ? Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                width: 100,
                child: ElevatedButton(
                  onPressed: controller.logoutPage,
                  child: Text("Logout"),
                ),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                width: 100,
                child: ElevatedButton(
                  onPressed: controller.registerPage,
                  child: Text("Cadastre-se"),
                ),
              ),
              SizedBox(height: 10, width: 10),
              SizedBox(
                width: 100,
                child: ElevatedButton(
                  onPressed: controller.loginPage,
                  child: Text("Login"),
                ),
              ),
            ],
          );
  }
}
