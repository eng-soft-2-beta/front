import 'package:flutter/material.dart';

class TabButtonWidget extends NavigationRailDestination {
  final IconData iconData;
  final String labelText;

  TabButtonWidget(this.labelText, this.iconData)
      : super(
          icon: SizedBox.shrink(),
          label: Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Column(
              children: [
                Icon(
                  iconData,
                  color: Colors.white70,
                ),
                Text(labelText,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white70)),
              ],
            ),
          ),
        );
}
