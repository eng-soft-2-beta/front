import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/modules/login/controllers/login_controller.dart';
import 'package:sharebook/app/shared/components/messages_form.dart';

import '../../../app_routes.dart';

class LoginPage extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    final imgVertical = MediaQuery.of(context).size.height / 2;
    final imgHorizontal = MediaQuery.of(context).size.width / 2;
    //final formVertical = MediaQuery.of(context).size.height / 5;
    // final formHorizontal = MediaQuery.of(context).size.width / 3;
    final titleFont = MediaQuery.of(context).size.width / 20;
    final subTitleFont = MediaQuery.of(context).size.width / 60;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.deepPurple,
              Colors.blue,
            ],
          ),
        ),
        child: Row(
          children: [
            Flexible(
              flex: 30,
              child: Column(
                children: [
                  Spacer(
                    flex: 10,
                  ),
                  Flexible(
                    flex: 25,
                    child: Text(
                      "ShareBook",
                      style: TextStyle(
                          fontFamily: 'ArchitectsDaughter',
                          fontSize: titleFont,
                          color: Colors.white),
                    ),
                  ),
                  Flexible(
                    flex: 10,
                    child: Text(
                      "Embarque em seu livro favorito!",
                      style: TextStyle(
                          fontFamily: 'ArchitectsDaughter',
                          fontSize: subTitleFont,
                          color: Colors.white),
                    ),
                  ),
                  Flexible(
                    flex: 55,
                    child: SizedBox(
                      height: imgVertical,
                      width: imgHorizontal,
                      child: Image.network('https://i.imgur.com/dCN6oOT.png'),
                    ),
                  ),
                ],
              ),
            ),
            Spacer(
              flex: 10,
            ),
            Flexible(
              flex: 60,
              child: Container(
                height: 250,
                width: 400,
                decoration: BoxDecoration(
                    border: Border.all(
                      width: 5.0,
                      color: Colors.indigo.shade100,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.indigo[50],
                    boxShadow: [
                      BoxShadow(color: Colors.black45, blurRadius: 30)
                    ]),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Form(
                      key: controller.chaveForm,
                      child: Column(
                        children: [
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            controller: controller.inputUsername,
                            decoration: InputDecoration(labelText: 'Username'),
                            validator: controller.emailIsValid,
                          ),
                          TextFormField(
                            controller: controller.inputPassword,
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: "Password",
                            ),
                            validator: controller.passwordIsValid,
                          ),
                          Container(
                            height: 20,
                            child: Obx(
                              () => ServerMessage.error(
                                  controller.loginError.value),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: ElevatedButton(
                              onPressed: () {
                                controller.login();
                              },
                              child: Text('Login'),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Get.toNamed(Routes.registerPage);
                            },
                            child: Text("Cadastre-se"),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
