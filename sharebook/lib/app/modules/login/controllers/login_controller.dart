import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql/client.dart';
import 'package:sharebook/app/modules/login/repositories/login_repository.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';

import '../../../app_routes.dart';

class LoginController extends GetxController {
  var loginError = ''.obs;
  final inputUsername = TextEditingController();
  final inputPassword = TextEditingController();
  final chaveForm = GlobalKey<FormState>();

  final LoginRepository _loginRepository;
  final TokenLocalStorage localStorage;

  LoginController(this._loginRepository, this.localStorage);

  login() async {
    loginError.value = '';
    if (chaveForm.currentState?.validate() ?? false) {
      chaveForm.currentState?.save();

      try {
        String token = await _loginRepository.login(
          this.inputUsername.text,
          this.inputPassword.text,
        );
        localStorage.saveToken(token);
        Get.offNamed(Routes.initial);
      } on GraphQLError catch (e) {
        print('Login GraphQLError: ${e.message}');
        loginError.value = e.message;
      } catch (e) {
        print('Login error: $e');
        loginError.value = "Error interno no servidor.";
      }
    }
  }

  logout() {
    localStorage.deleteToken();
    Get.offNamed(Routes.initial);
  }

  String? emailIsValid(String? value) {
    if (!GetUtils.isEmail(value ?? "")) return 'Insira um email válido!';
    return null;
  }

  String? passwordIsValid(String? value) {
    if (value?.isEmpty ?? false) return 'Insira sua senha!';
    return null;
  }
}
