import 'package:graphql/client.dart';
import 'package:sharebook/app/models/book_model.dart';
import 'package:sharebook/app/shared/storage/local_storage.dart';

class BooksRepository {
  final GraphQLClient client;
  final TokenLocalStorage storage;

  BooksRepository(this.client, this.storage);

  Future<BookModel> registerBook(BookModel book) async {
    var query = """
      mutation AddBook(\$isbn: String!, \$barCode: String!,
        \$title: String!, \$autor: String!, \$publishingCompany: String!){
          addBook(bookInput:{isbn:\$isbn,
            barCode:\$barCode,
            title:\$title,
            autor:\$autor,
            publishingCompany:\$publishingCompany
          }){
          book{
            isbn
            id
            title
            autor
            publishingCompany
            status
          }
        }
      }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: {
        "isbn": book.isbn,
        "barCode": "",
        "title": book.title,
        "autor": book.autor,
        "publishingCompany": book.publishingCompany,
      },
    );

    final QueryResult result = await client.mutate(options);

    var response = result.data?["addBook"]?["book"];
    return BookModel.fromMap(response);
  }

  Future<String> removeBook(String bookId) async {
    var query = """
    mutation RemoveBook(\$bookId: String!){
      removeBook(bookId:\$bookId){
        book{
          id
        }
      }
    }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: {"bookId": bookId},
    );

    final QueryResult result = await client.mutate(options);

    var _token = result.data?["tokenAuth"]?["token"];
    return _token ?? "";
  }

  Future<BookModel> updateBook(BookModel book) async {
    var query = """
      mutation UpdateBook(\$bookId: String!,\$isbn: String!, \$barCode: String!,
        \$title: String!, \$autor: String!, \$publishingCompany: String!){
        updateBook(bookId: \$bookId,bookInput:{isbn:\$isbn,
          barCode:\$barCode,
          title:\$title,
          autor:\$autor,
          publishingCompany:\$publishingCompany
        }){
            book{
            isbn
            id
            title
            autor
            publishingCompany
            status
          }
        }
      }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: {
        "bookId": book.id,
        "isbn": book.isbn,
        "barCode": "",
        "title": book.title,
        "autor": book.autor,
        "publishingCompany": book.publishingCompany,
      },
    );

    final QueryResult result = await client.mutate(options);

    var response = result.data?["updateBook"]?["book"];
    return BookModel.fromMap(response);
  }

  Future<String> updateStatus(int? bookId, String? status) async {
    var query = """
    mutation UpdateBookStatus(\$bookId: String!,\$bookStatus: String!){
      updateStatus(bookId:\$bookId, bookStatus:\$bookStatus){
        book{
          id
          status
        }
      }
    }
    """;

    final MutationOptions options = MutationOptions(
      document: gql(query),
      variables: {"bookId": bookId.toString(), "bookStatus": status},
    );

    final QueryResult result = await client.mutate(options);

    var _token = result.data?["tokenAuth"]?["token"];
    return _token ?? "";
  }

  Future<List<BookModel>> listBooks() async {
    var query = """
      query listLivros {
        getPerson{
          library {
            books {
              id
              title
              autor
              publishingCompany
              isbn        
              status
            }
          }
        }
      }
    """;

    final QueryOptions options = QueryOptions(document: gql(query));

    final QueryResult result = await client.query(options);

    var response = result.data?["getPerson"]?[0]["library"]["books"];
    return List<BookModel>.from(response.map((e) => BookModel.fromMap(e)));
  }
}
