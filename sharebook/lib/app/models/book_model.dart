import 'dart:convert';

class BookModel {
  BookModel({
    this.id,
    this.isbn,
    this.title,
    this.autor,
    this.publishingCompany,
    this.status,
    //this.emprestimos
  });

  int? id;
  String? isbn;
  String? title;
  String? autor;
  String? publishingCompany;
  String? status;
  //TODO Adicionar lista de emprestimos.

  factory BookModel.fromJson(String str) => BookModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BookModel.fromMap(Map<String, dynamic> json) => BookModel(
        id: json["id"] == null ? null : int.parse(json["id"]),
        isbn: json["isbn"] == null ? null : json["isbn"],
        title: json["title"] == null ? null : json["title"],
        autor: json["autor"] == null ? null : json["autor"],
        publishingCompany: json["publishingCompany"] == null
            ? null
            : json["publishingCompany"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "isbn": isbn == null ? null : isbn,
        "title": title == null ? null : title,
        "autor": autor == null ? null : autor,
        "publishingCompany":
            publishingCompany == null ? null : publishingCompany,
        "status": status == null ? null : autor,
      };

  bool get isEmprestado => status == "EMPRESTADO";
  bool get isDisponivel => status == "DISPONIVEL";
  bool get isPrivado => status == "PRIVADO";

  String toString() {
    return "$id - $title - $status";
  }
}
