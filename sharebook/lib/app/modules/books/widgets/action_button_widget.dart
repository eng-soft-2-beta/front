import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final onPressed;

  const ActionButton({Key? key, required this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      width: 200.0,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 1.0,
            color: Colors.grey,
            offset: Offset(
              1.0,
              3.0,
            ),
          )
        ],
        color: Colors.lightBlue,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.02),
      child: TextButton(
        child: Text(
          'Cadastrar Livro',
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
