import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sharebook/app/modules/register/controllers/register_controller.dart';

class RegisterPage extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.deepPurple,
                  Colors.blue,
                ],
              ),
            ),
          ),
          Center(
            child: Container(
              width: 500,
              height: 400,
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 5.0,
                    color: Colors.indigo.shade100,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.indigo[50],
                  boxShadow: [
                    BoxShadow(color: Colors.black45, blurRadius: 30)
                  ]),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Form(
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(labelText: 'Nome completo'),
                        onChanged: (value) {
                          controller.name.value = value;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'CPF'),
                        onChanged: (value) {
                          controller.cpf.value = value;
                        },
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'CNH'),
                      ),
                      TextFormField(
                        decoration: InputDecoration(labelText: 'E-mail'),
                        validator: (String? value) {
                          if (!controller.emailIsValid(value))
                            return 'Insira um email válido!';
                          return null;
                        },
                        onChanged: (value) {
                          controller.username.value = value;
                        },
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Senha",
                        ),
                        onChanged: (value) {
                          controller.password.value = value;
                        },
                      ),
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Confirmar Senha",
                        ),
                        validator: (value) {
                          if (value != controller.password.value) {
                            return "As senhas dever ser iguais";
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: ElevatedButton(
                          onPressed: () {
                            controller.singup();
                          },
                          child: Text('Cadastrar'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
